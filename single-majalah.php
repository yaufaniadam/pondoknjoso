<?php get_header(); ?>

  <!-- news-area-start -->
        <div class="news-area hero-padding pt-3 pt-md-5 pb-3 pb-md-4">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-8">
                        <div class="layout-wrapper layout-wrapper-02 mb-30">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="majalah">
                                    
                                    <?php 
                                            if ( has_post_thumbnail() ) {
                                                the_post_thumbnail('magazine');                     
                                            } else {
                                        ?>                                                              
                                            <img src="<?php bloginfo('template_directory');?>/images/hero/11.jpg" alt="<?php the_title(); ?>" />
                                    <?php } ?>
                                    
                                    <div class="pt-5">
                                        <div class="section-title mb-1 mb-lg-4">
                                            <h4>Info Pemesanan</h4>
                                        </div>
                                        <ul class="">                                   
                                            <li><span><i class="far fa-phone"></i> (0274) 373263</span></li>
                                            <li><span><i class="fab fa-whatsapp"></i>  0817 270 787</span></li>
                                            <li><span><i class="far fa-envelope-open"></i> suaraaisyiyah@aisyiyah.or.id</span></li>
                                        </ul>
                                    </div>
                                   
                                   
                                </div>
                            </div>
                            <div class="col-md-8">
                            <div class="lifestyle-wrapper mb-35">
                                <div class="lifestyle-text lifestyle-02-text">
                                    
                                    <?php /* untuk statistik view */ setPostViews(get_the_ID()); ?>
                                    <!--<span class="post-box-cart1 mr-20"><a href="#"><i class="far fa-book-open"></i> <?php echo number_format(getPostViews(get_the_ID())); ?>x</a></span>-->

                                    <h4><a href="<?php the_permalink(); ?>">Majalah Suara 'Aisyiyah <?php the_title(); ?></a></h4>
                                   
                                    <?php if(have_posts()) : while ( have_posts() ) : the_post();?>
                                        <div class="post">   
                                            <?php the_content(); ?>             
                                        </div>
                                    <?php  endwhile; endif; ?>           
                                </div>
                            </div>               

                            </div>
                        </div>



                            
                           
                            
                             
                            <div class="row">
                                <div class="col-xl-8 col-lg-7 col-md-6">
                                    <div class="blog-post-tag">
                                    <?php $tags = get_the_tags($post->ID); 
                                        if($tags) : ?>
                                        <span>Tag : </span>
                                                 
                                         
                                        <?php foreach($tags as $tag) :  ?>                        
                                            <a href="<?php bloginfo('url');?>/tag/<?php print_r($tag->slug);?>"><?php print_r($tag->name); ?></a>,   
                                        <?php endforeach; endif; ?>                                        
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-5 col-md-6">
                                    <div class="blog-share-icon text-left text-md-right">
                                        <!--[ssba]-->
                                    </div>
                                </div>
                            </div>                        
                        </div>
                    </div>
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>

<?php get_footer(); ?>
