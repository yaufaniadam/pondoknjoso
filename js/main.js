(function ($) {
	"use strict";

	// meanmenu
	$('#mobile-menu').meanmenu({
		meanMenuContainer: '.mobile-menu',
		meanScreenWidth: "992"
	});

	$('.info-bar').on('click', function () {
		$('.extra-info').addClass('info-open');
	})

	$('.close-icon').on('click', function () {
		$('.extra-info').removeClass('info-open');
	})


	// sticky
	var wind = $(window);
	var sticky = $('#sticky-header');
	wind.on('scroll', function () {
		var scroll = wind.scrollTop();
		if (scroll < 100) {
			sticky.removeClass('sticky');
		} else {
			sticky.addClass('sticky');
		}
	});


/* breaking-active owl untuk quotes */
$('.breaking-2-active').owlCarousel({
	loop: true,
	nav: true,
	dots: false,
	lazyLoad:true,
	margin:0,
	autoplay: true,
	navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
	responsive: {
		0: {
			items: 1,
			nav: false,
		},
		576: {
			items: 1,
			nav: false,
		},
		768: {
			items: 1,
			nav: false,
		},
		992: {
			items: 1,
			nav: false,
		},
		1200: {
			items: 1
		}
	}
})



/* news-active owl untuk majalah */

$('.news-active').owlCarousel({
	loop: true,
	nav: true,
	dots: false,
	lazyLoad:true,
	margin:0,
	autoplay: false,
	navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
	responsive: {
		0: {
			items: 1,
			nav: true,
		},
		576: {
			items: 1,
			nav: false,
		},
		768: {
			items: 2
		},
		992: {
			items: 2
		},
		1200: {
			items: 3
		}
	}
})

/* news-active */ /*berita populer */
$('.news-04-active').owlCarousel({
	loop: true,
	nav: true,
	dots: false,
	lazyLoad:true,
	margin:0,
	autoplay: false,
	navText: ['<i class="fas fa-chevron-left"></i>', '<i class="fas fa-chevron-right"></i>'],
	responsive: {
		0: {
			items: 1,
			nav: true,
		},
		576: {
			items: 1,
			nav: true,
		},
		768: {
			items: 2
		},
		992: {
			items: 3
		},
		1200: {
			items: 5
		}
	}
})


	// scrollToTop
	$.scrollUp({
		scrollName: 'scrollUp', // Element ID
		topDistance: '300', // Distance from top before showing element (px)
		topSpeed: 300, // Speed back to top (ms)
		animation: 'fade', // Fade, slide, none
		animationInSpeed: 200, // Animation in speed (ms)
		animationOutSpeed: 200, // Animation out speed (ms)
		scrollText: '<i class="fas fa-angle-up"></i>', // Text for element
		activeOverlay: false, // Set CSS color to display scrollUp active point, e.g '#00FFFF'
	});


	})(jQuery);