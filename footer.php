       
            <!-- banner-Home 3-->
            <div class="banner-area pb-2 pb-md-4">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-8">
                            <div class="banner-2-img">
                                <a href="#"><img src="<?php bloginfo('template_directory');?>/images/iklan/banner-muktamar4.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- banner-area-end -->
        </main>
        <!-- footer-area-start -->
        <footer>
            <div class="footer-area darkgrey-bg pt-4 pt-lg-5">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-4 col-lg-6 col-md-6">
                            <div class="footer-wrapper mb-20">
                                <h3 class="footer-title">Tentang Suara 'Aisyiyah</h3>
                                <div class="footer-text">
                                    <p>Suara Aisyiyah adalah majalah wanita tertua di Indonesia.  Terbit sejak tahun 1926 sampai sekarang, perkembangannya dapat diikuti sejak zaman kolonial Belanda, zaman Jepang hingga zaman kemerdekaan.</p>
                                </div>
                                <div class="social-icon footer-icon">
                                    <a class="fb" href="https://www.facebook.com/majalahsuaraaisyiyah"><i class="fab fa-facebook-f"></i></a>
                                    <a class="twit" href="https://twitter.com/aisyiyahsuara"><i class="fab fa-twitter"></i></a>
                                    <a class="insta" href="https://www.instagram.com/suaraaisyiyah/"><i class="fab fa-instagram"></i></a>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-6">
                            <div class="footer-wrapper pl-30 mb-30">
                                <h3 class="footer-title">Informasi</h3>
                                    <?php 
                                        wp_nav_menu( array(
                                            'menu'              => 'footer1',
                                            'theme_location'    => 'footer1',
                                            'depth'             => 1,
                                            'container'         => 'div',
                                            'menu_class'    => 'footer-link',
                                            )
                                        ); 
                                    ?>                                 
                            </div>
                        </div>
                        <div class="col-xl-2 col-lg-3 col-md-3">
                            <div class="footer-wrapper pl-25 mb-30">
                                <h3 class="footer-title">Majalah</h3>
                                <?php 
                                        wp_nav_menu( array(
                                            'menu'              => 'footer2',
                                            'theme_location'    => 'footer2',
                                            'depth'             => 1,
                                            'container'         => 'div',
                                            'menu_class'    => 'footer-link',
                                            )
                                        ); 
                                    ?>  

                            </div>
                        </div>
                        <div class="col-xl-4 col-lg-4 col-md-4">
                            <div class="footer-wrapper mb-30">
                                <h3 class="footer-title">Kontak Kami</h3>
                                <ul class="footer-info">
                                    <li><span><i class="far fa-map-marker-alt"></i> Jl. Kauman Gm I/7A Yogyakarta 55122</span></li>
                                    <li><span><i class="far fa-envelope-open"></i> suaraaisyiyah@aisyiyah.or.id</span></li>
                                    <li><span><i class="far fa-phone"></i> (0274) 373263</span></li>
                                     <li><span><i class="fab fa-whatsapp"></i>  0817 270 787</span></li>
                                    <li><span><i class="far fa-map"></i> <a href="">Peta Google</a></span></li>
                                </ul>
                            </div>
                        </div>
                        
                    </div>
                    <div class="footer-bottom-area mt-2 pt-25 pb-25 footer-border-top">
                        <div class="row">
                            <div class="col-xl-8 col-lg-12 offset-xl-2">
                                
                                <div class="copyright text-center">
                                    <p>Copyright <i class="far fa-copyright"></i> 2020 <a href="<?php bloginfo('url');?>"> Majalah Suara 'Aisyiyah</a></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- footer-area-end -->

         <!-- Modal Search -->
         <div class="modal fade" id="search-modal" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">      
                    <form role="search" method="get" id="searchform" action="<?php home_url( '/' ); ?>" >
                        <input type="text" value="<?php get_search_query(); ?>" placeholder="Ketikkan di sini ..." name="s" id="s">
                        <input type="hidden" name="post_type" value="post" />
                        <button>
                            <i class="fa fa-search"></i>
                        </button>
                    </form>
                </div>
            </div>
        </div>

		<?php wp_footer(); ?>

    </body>
</html>