<?php get_header(); ?>

  <!-- news-area-start -->
        <div class="news-area hero-padding pt-3 pt-md-5 pb-3 pb-md-4">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-8">
                        <div class="row">
                            <div class="col-xl-12 col-lg-12 mb-2">
                                <h4>Indeks</h4> 
                               
                            </div>
                        </div>   
                          
                        <?php if(have_posts()) : while ( have_posts() ) : the_post();?>

                            <div class="row">
                                <div class="col-xl-5 col-lg-6 mb-30">
                                    <div class="lifestyle-img">
                                        <?php 
                                            if ( has_post_thumbnail() ) {
                                                the_post_thumbnail('latest');                     
                                            } else {
                                        ?>                                                                  
                                                <img src="<?php bloginfo('template_directory');?>/images/noimage/latest.jpg" alt="<?php the_title(); ?>" />
                                        <?php 
                                            }                       
                                        ?>
                                    </div>
                                </div>
                                <div class="col-xl-7 col-lg-6 mb-30">
                                    <div class="lifestyle-text lifestyle-04-text">
                                        
                                        <div class="post-content-meta"><?php nama_kategori(); ?>
                                            <span><a class="meta-11" href="#"><i class="far fa-clock"></i> <?php echo get_the_date(); ?></a></span>
                                            <span><a class="meta-11" href="#" title="<?php echo $post->comment_count; ?> komentar Pembaca"><i class="far fa-comment"></i> (<?php echo $post->comment_count; ?>)</a></span>
                                        </div>
                                        <h4><a href="<?php the_permalink(); ?>" title="Baca selengkapnya : <?php the_title(); ?>"><?php the_title(); ?></a></h4>
                                        <p><?php echo excerpt(35); ?></p>
                                        
                                    </div>
                                </div>
                            </div>

                        <?php  endwhile; 

                        else : ?>

                             <div class="row">
                                <div class="col-xl-12 col-lg-12 mb-30">
                                    <div class="post">
                                        <h3>Belum ada artikel dengan kategori: <?php echo get_cat_name( get_query_var( 'cat' )); ?></h3>
                                        <hr>                                       
                                        
                                      
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <?php hero_sidebar('Artikel Terbaru','', '5'); ?>
                                            </div>
                                            <div class="col-lg-6">
                                                <?php hero_sidebar('Berita Terbaru','berita', '5'); ?>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>


                        <?php endif; ?>                                  
                        <div class="row">
                            <div class="col-xl-5 col-lg-6 mb-10 pb-3">
                                <?php wp_pagenavi(); ?>
                            </div>
                        </div> 
                    </div>
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>

<?php get_footer(); ?>
