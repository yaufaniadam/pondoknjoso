<?php get_header(); ?>

  <!-- news-area-start -->
        <div class="news-area  hero-padding pt-80 pb-50">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-8">
                        <div class="layout-wrapper layout-wrapper-02 mb-30">
                            <div class="lifestyle-wrapper mb-35">
                                <h2 style="font-size:70px;"><i class='fas fa-exclamation-triangle'></i> 404</h2>
                                <h3>Mohon maaf artikel yang Anda cari tidak ditemukan.</h3>
                                <hr>
                                <div class="post">                                       
                                        
                                        <div class="section-title mb-30">
                                            <p>Silakan kembali ke <a href="<?php bloginfo('url'); ?>">Home</a> atau baca artikel di bawah: </p>
                                        </div>
                                       
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <?php hero_sidebar('Artikel Terbaru','', '5'); ?>
                                            </div>
                                            <div class="col-lg-6">
                                                <?php hero_sidebar('Berita Terbaru','berita', '5'); ?>
                                            </div>
                                        </div>
                                        

                                    </div>
                            </div>
                           
                            
                            
                                                   
                        </div>
                    </div>
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>

<?php get_footer(); ?>
