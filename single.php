<?php get_header(); ?>

  <!-- news-area-start -->
        <div class="news-area hero-padding pt-3 pt-md-5 pb-3 pb-md-4">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-8">
                        <div class="layout-wrapper layout-wrapper-02 mb-30 pl-0 pl-md-3">
                            <div class="lifestyle-wrapper mb-35">
                                <div class="lifestyle-text lifestyle-02-text">
                                    
                                    <?php if(have_posts()) : while ( have_posts() ) : the_post();?>
                                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                    
                                        <span class="mr-0"><?php nama_kategori(); ?></span>
                                        
                                        <span class="post-box-cart1 mr-2 mr-lg-4"><a href="#"><i class="far fa-clock"></i> <?php echo get_the_date('j M Y'); ?></a></span>
                                        <span class="post-box-cart1 mr-2 mr-lg-4"><a href="#" title="<?php echo $post->comment_count; ?> komentar Pembaca"><i class="far fa-comment"></i> <?php echo $post->comment_count; ?></a></span>

                                        <?php /* untuk statistik view */ setPostViews(get_the_ID()); ?>
                                        <span class="post-box-cart1 mr-2 mr-lg-4"><a href="#" title="Telah dibaca <?php echo number_format(getPostViews(get_the_ID())); ?> kali"><i class="far fa-book-open"></i> <?php echo number_format(getPostViews(get_the_ID())); ?>x</a></span>

                                        <div class="post pt-3">   
                                            <?php the_content(); ?>             
                                        </div>
                                    <?php  endwhile; endif; ?>           
                                </div>
                            </div>
                           
                            
                             
                            <div class="row">
                                <div class="col-xl-8 col-lg-7 col-md-6">
                                    <div class="blog-post-tag">
                                    <?php $tags = get_the_tags($post->ID); 
                                        if($tags) : ?>
                                        <span>Tag : </span>
                                                 
                                         
                                        <?php foreach($tags as $tag) :  ?>                        
                                            <a href="<?php bloginfo('url');?>/tag/<?php print_r($tag->slug);?>"><?php print_r($tag->name); ?></a>,   
                                        <?php endforeach; endif; ?>                                        
                                    </div>
                                </div>
                                <div class="col-xl-4 col-lg-5 col-md-6">
                                    <div class="blog-share-icon text-left text-md-right">
                                        <?php echo do_shortcode('[ssba-buttons]'); ?>
                                    </div>
                                </div>
                            </div>  
                            <div class="row">
                                <div class="col-md-12">
                                   <?php
                                    // If comments are open or we have at least one comment, load up the comment template.
                                    if ( comments_open() || get_comments_number() ) {
                                        comments_template();
                                    } ?>
                                </div>
                            </div>                      
                        </div>
                    </div>
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>

<?php get_footer(); ?>
