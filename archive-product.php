<?php get_header(); ?>

  <!-- news-area-start -->
        <div class="news-area hero-padding pt-3 pt-md-5 pb-3 pb-md-4">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-8">
                        <div class="row">
                            <div class="col-xl-5 col-lg-6 mb-10">
                                <h4>Produk Suara 'Aisyiyah</h4> 
                               
                            </div>
                        </div>   
                        <div class="row">   
                        <?php if(have_posts()) : while ( have_posts() ) : the_post();?>

                        <div class="col-md-4 col-6">
                            <div class="hero-wrapper hero-04-wrapper mb-30">
                                <div class="hero-img pos-rel majalah">
                                    <?php 
                                        if ( has_post_thumbnail() ) {
                                            the_post_thumbnail('magazine');                     
                                        } else {
                                    ?>                                                              
                                        <img src="<?php bloginfo('template_directory');?>/images/noimage/magazine.jpg" alt="<?php the_title(); ?>" />
                                    <?php } ?>
                                   
                                  
                                    <div class="hero-text">                                        
                                    <h3><a href="<?php the_permalink(); ?>" title="Baca selengkapnya : <?php the_title(); ?>"><?php the_title(); ?></a></h3>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php  endwhile; 
                          endif; ?>   
                        </div> <!-- end row majalah -->                               
                        <div class="row">
                            <div class="col-xl-5 col-lg-6 mb-10">
                                <?php wp_pagenavi(); ?>
                            </div>
                        </div> 
                    </div>
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>

<?php get_footer(); ?>
