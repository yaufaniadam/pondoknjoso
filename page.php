<?php get_header(); ?>

  <!-- news-area-start -->
        <div class="news-area hero-padding pt-3 pt-md-5 pb-3 pb-md-4">
            <div class="container">
                <div class="row">
                    <div class="col-xl-8 col-lg-8">
                        <div class="layout-wrapper layout-wrapper-02 mb-30 pl-0 pl-md-3">
                            <div class="lifestyle-wrapper mb-35">
                                <div class="lifestyle-text lifestyle-02-text">
                                    
                                    <?php if(have_posts()) : while ( have_posts() ) : the_post();?>
                                        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
                                    
                                      
                                        
                                      

                                        

                                        <div class="post pt-3">   
                                            <?php the_content(); ?>             
                                        </div>
                                    <?php  endwhile; endif; ?>           
                                </div>
                            </div>
                           
                            
                             
                           
                                            
                        </div>
                    </div>
                    <?php get_sidebar(); ?>
                </div>
            </div>
        </div>

<?php get_footer(); ?>
