<?php
require_once('inc/better-excerpts.php');
require_once('inc/cpt.php');
require_once('inc/wp_bootstrap_navwalker.php');

//require_once( '3rdparty/tjd-widget/tjd-widget.php' );

if ( ! function_exists( 'suaraaisyiyah_theme_setup' ) ) :
function suaraaisyiyah_theme_setup() {	
		
	add_theme_support( 'automatic-feed-links' );
	
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'headline', 664, 684, true );
	add_image_size( 'headline-mini', 318, 210, true );
	add_image_size( 'popular', 248, 281, true );
	add_image_size( 'latest', 348, 240, true );
	add_image_size( 'magazine', 433, 569, true ); 
	add_image_size( 'thumbnail-big', 432, 313,true );
	add_image_size( 'thumbnail', 150, 150,true );
	add_image_size( 'thumbnail-square', 120, 91,true );

	register_nav_menus( array(
		'top-header'   => __( 'Menu header bagian atas', 'suaraaisyiyah_theme' ),
		'primary'   => __( 'Menu Utama', 'suaraaisyiyah_theme' ),
		'footer1'   => __( 'Footer menu 1', 'suaraaisyiyah_theme' ),
		'footer2'   => __( 'Footer menu 2', 'suaraaisyiyah_theme' ),
	) );

	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list',
	) );

	add_filter( 'use_default_gallery_style', '__return_false' );	
	
}
endif; // suaraaisyiyah_theme_setup
add_action( 'after_setup_theme', 'suaraaisyiyah_theme_setup' );

add_filter( 'wp_enqueue_scripts', 'suaraaisyiyah_theme_scripts',0 );

function suaraaisyiyah_theme_scripts() {


	// CSS
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css', array(), '1.0' );
    wp_enqueue_style( 'owl', get_template_directory_uri() . '/css/owl.carousel.min.css', array(), '1.0' );
    wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.min.css', array(), '1.0' );
 //   wp_enqueue_style( 'magnific-popup', get_template_directory_uri() . '/css/magnific-popup.css', array(), '1.0' );
    wp_enqueue_style( 'fontawesome-all', get_template_directory_uri() . '/css/fontawesome-all.min.css', array(), '1.0' );
 //   wp_enqueue_style( 'themify-icons', get_template_directory_uri() . '/css/themify-icons.css', array(), '1.0' );
    wp_enqueue_style( 'meanmenu', get_template_directory_uri() . '/css/meanmenu.css', array(), '1.0' );
//    wp_enqueue_style( 'slick', get_template_directory_uri() . '/css/slick.css', array(), '1.0' );
	wp_enqueue_style( 'main', get_template_directory_uri() . '/css/main.min.css', array(), '1.0' );
	wp_enqueue_style( 'news', get_template_directory_uri() . '/css/news.min.css', array(), '1.0' );	
	wp_enqueue_style( 'responsive', get_template_directory_uri() . '/css/responsive.css', array(), '1.0' );
	wp_enqueue_style( 'suaraaisyiyah_theme-style', get_stylesheet_uri(), array() );	

	// JS
    wp_register_script( 'modernizr-3', get_template_directory_uri() . '/js/vendor/modernizr-3.5.0.min.js', array(), '1.0',true );	
	wp_register_script( 'popper', get_template_directory_uri() . '/js/popper.min.js', array('jquery'), '1.0',true );	
	wp_register_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '1.0',true );	
	wp_register_script( 'owl', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '1.0',true );
	//wp_register_script( 'isotope', get_template_directory_uri() . '/js/isotope.pkgd.min.js', array('jquery'), '1.0',true );
//	wp_register_script( 'slick', get_template_directory_uri() . '/js/slick.min.js', array('jquery'), '1.0',true );
	wp_register_script( 'meanmenu', get_template_directory_uri() . '/js/jquery.meanmenu.min.js', array('jquery'), '1.0',true );
//	wp_register_script( 'ajax-form', get_template_directory_uri() . '/js/ajax-form.js', array('jquery'), '1.0',true );
	//wp_register_script( 'wow', get_template_directory_uri() . '/js/wow.min.js', array('jquery'), '1.0',true );
	wp_register_script( 'scrollUp', get_template_directory_uri() . '/js/jquery.scrollUp.min.js', array('jquery'), '1.0',true );
//	wp_register_script( 'counterup', get_template_directory_uri() . '/js/jquery.counterup.min.js', array('jquery'), '1.0',true );
	wp_register_script( 'waypoints', get_template_directory_uri() . '/js/waypoints.min.js', array('jquery'), '1.0',true );
//	wp_register_script( 'imagesloaded', get_template_directory_uri() . '/js/imagesloaded.pkgd.min.js', array('jquery'), '1.0',true );
//	wp_register_script( 'magnific-popup', get_template_directory_uri() . '/js/jquery.magnific-popup.min.js', array('jquery'), '1.0',true );	
	wp_register_script( 'plugins', get_template_directory_uri() . '/js/plugins.js', array('jquery'), '1.0',true );	
	wp_register_script( 'main-js', get_template_directory_uri() . '/js/main.js', array('jquery'), '1.0',true );	

	wp_enqueue_script( 'modernizr-3' );
	wp_enqueue_script( 'popper' );
	wp_enqueue_script( 'bootstrap' );
	wp_enqueue_script( 'owl' );
//	wp_enqueue_script( 'isotope' );
//	wp_enqueue_script( 'slick' );
	wp_enqueue_script( 'meanmenu' );
//	wp_enqueue_script( 'ajax-form' );
//	wp_enqueue_script( 'wow' );
	wp_enqueue_script( 'scrollUp' );
//	wp_enqueue_script( 'counterup' );
	wp_enqueue_script( 'waypoints' );
//	wp_enqueue_script( 'imagesloaded' );
//	wp_enqueue_script( 'magnific-popup' );
	wp_enqueue_script( 'plugins' );
	wp_enqueue_script( 'main-js' );
	
}

function suaraaisyiyah_theme_title( $title, $sep ) {
	global $paged, $page;

	if ( is_feed() ) {
		return $title;
	}

	$title .= get_bloginfo( 'name' );

	$site_description = get_bloginfo( 'description', 'display' );
	if ( $site_description && ( is_home() || is_front_page() ) ) {
		$title = "$title $sep $site_description";
	}

	if ( $paged >= 2 || $page >= 2 ) {
		$title = "$title $sep " . sprintf( __( 'Page %s', 'suaraaisyiyah' ), max( $paged, $page ) );
	}

	return $title;
}
add_filter( 'wp_title', 'suaraaisyiyah_theme_title', 10, 2 );

function sc_breadcrumb_trail() {
	if ( function_exists('breadcrumb_trail') ) {
		breadcrumb_trail('show_browse=false');
	}
}
add_shortcode( 'sc_breadcrumb_trail', 'sc_breadcrumb_trail' );



function kutipan () {
	?>

	<!-- kutipan -->
            <div class="breaking-2-area pt-20 pb-0 pb-md-4 d-none d-md-block">
                <div class="container">
                    <div class="breaking-2-border">
                        <div class="row align-items-center">
                            <div class="col-xl-12">
                               <div class="header-wrapper">
                                   <div class="header-info mr-40 f-left">
                                       <h5><i class="fas fa-quote-right"></i></h5>
                                   </div>
                                   <div class="header-text quote">
                                       <ul class="breaking-2-active owl-carousel">
                                       	<?php
			
											$the_query = null;
											$the_query = new WP_Query('post_status=publish&post_type=quotes&posts_per_page=5&orderby=rand'); 
											while ( $the_query->have_posts() ) : $the_query->the_post();

										?>
                                           <li><a href="#"><strong><?php the_title(); echo "</strong> <em>(". get_field( "nama_tokoh" ); ?>)</em></a></li>

                                         <?php endwhile; ?>

                                       </ul>
                                   </div>
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- kutipan end -->
<?php            
}

add_filter('the_category','add_class_to_category',10,3);

function add_class_to_category( $thelist, $separator, $parents){
    $class_to_add = 'meta-1 meta-3';
    return str_replace('<a href="', '<a class="' . $class_to_add . '" href="', $thelist);
}



function getPostViews($postID){

    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);

    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count;
}

function setPostViews($postID) {

    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

function hero_sidebar($judul, $category_name, $post_per_page) {

?>
<div class="hero-sidebar-item mb-10">
    <div class="section-title mb-3 mb-lg-4">
        <h4><?php echo $judul; ?></h4>
    </div>
    <div class="hero-sidebar">
        <div class="hero-post-item hero-post-item-2">      

            <?php     
                
                $args = array (
                    'post_status'   =>'publish',
                    'post_type'     =>'post',
                    'category_name' => $category_name,
                    'posts_per_page'=> $post_per_page,
                );

				$the_query = null;
				$the_query = new WP_Query(); 
				$the_query->query( $args );
                while ( $the_query->have_posts() ) : $the_query->the_post();
            ?>         

                <div class="post-sm-list fix mb-20">
                    <div class="post-sm-img f-left">                                
                        <?php 
                            if ( has_post_thumbnail() ) {
                                the_post_thumbnail('thumbnail-square');                     
                            } else {
                        ?>                                                                  
	                            <img src="<?php bloginfo('template_directory');?>/images/noimage/thumbnail-square.jpg" alt="<?php the_title(); ?>" />
                        <?php 
                            }                       
                        ?>
                    </div>
	                <div class="post-2-content fix">
                        <div class="post-content-meta">
                            <span><?php nama_kategori_text(); ?></span>
                            <span><a class="meta-11" href="#"><i class="far fa-clock"></i> <?php echo get_the_date('j M Y'); ?></a></span>
                        </div>
                            <h4><a href="<?php the_permalink(); ?>" title="Baca selengkapnya : <?php the_title(); ?>"><?php the_title(); ?></a></h4>
                    </div>
                </div>
                <?php endwhile; $wp_query = null;  $wp_query = $temp; ?>                                   
        </div>       
    </div>
</div>

<?php 

}

function hero_sidebar2($judul, $category_name, $post_per_page) {

	?>
	<div class="hero-sidebar-item mb-10">
		<div class="section-title mb-15 mb-lg-4">
			<h4><?php echo $judul; ?></h4>
		</div>
		<div class="hero-sidebar">
	
			<div class="hero-post-item hero-post-item-2">			
	
				<?php				  
					$the_query = null;
					$args = array (
						'post_status'   =>'publish',
						'post_type'     =>'post',
						'category_name' => $category_name,
						'posts_per_page'=> $post_per_page,
					);
	
					$the_query = new WP_Query($args); 
					while ( $the_query->have_posts() ) : $the_query->the_post();

					if( $the_query->current_post == 0 ) { ?>

					<div class="hero-wrapper hero-03-wrapper mb-20">
						<div class="hero-img pos-rel">

							<?php 
								if ( has_post_thumbnail() ) {
									the_post_thumbnail('thumbnail-big');                     
								} else {
							?>                                                                  
									<img src="<?php bloginfo('template_directory');?>/images/noimage/thumbnail-big.jpg" alt="<?php the_title(); ?>" />
							<?php 
								}                       
							?>
						
							<span><?php nama_kategori_headline(); ?></span>
							<div class="hero-text">
								<div class="hero-meta">
									<span><a href="#"><i class="far fa-clock"></i> <?php echo get_the_date(); ?></a></span>
								
								</div>
								<h3><a href="<?php the_permalink(); ?>" title="Baca selengkapnya : <?php the_title(); ?>"><?php the_title(); ?></a></h3>
								
							</div>
						</div>
					</div> 

					<?php } else { ?>			  
					
					<div class="post-sm-list fix mb-20">
						<div class="post-sm-img f-left">                                
							<?php 
								if ( has_post_thumbnail() ) {
									the_post_thumbnail('thumbnail-square');                     
								} else {
							?>                                                                  
									<img src="<?php bloginfo('template_directory');?>/images/noimage/thumbnail-square.jpg" alt="<?php the_title(); ?>" />
							<?php 
								}                       
							?>
						</div>
						<div class="post-2-content fix">
							<div class="post-content-meta">
								<span><?php nama_kategori_text(); ?></span>
								<span><a class="meta-11" href="#"><i class="far fa-clock"></i> <?php echo get_the_date(); ?></a></span>
							</div>
								<h4><a href="<?php the_permalink(); ?>" title="Baca selengkapnya : <?php the_title(); ?>"><?php the_title(); ?></a></h4>
						</div>
					</div>
					<?php } // end loop
				endwhile; wp_reset_query(); ?>                                   
			</div>		   
		</div>
	</div>	
	<?php 	
}

function nama_kategori() {

	$terms = get_the_terms($post->ID, 'category');
	if($terms) :
	foreach( $terms as $category) { 

		$warna_kategori = get_term_meta($category->term_id, 'warna_kategori', true);
		$warna_kategori = ($warna_kategori !=='') ? $warna_kategori  : '#333333';
		?>

		<span class="post-box-cart"><a title="Lihat artikel lainnya pada kategori <?php echo $category->name ; ?>" style="color:<?php echo $warna_kategori; ?>" href="<?php echo get_category_link( $category->term_id ); ?>" >
		<i class="fas fa-caret-right"></i> <?php echo $category->name ; ?></a></span>
		

	<?php }
	endif;
}

function nama_kategori_headline() {

	$terms = get_the_terms($post->ID, 'category');
	foreach( $terms as $category) { 

		$warna_kategori = get_term_meta($category->term_id, 'warna_kategori', true);
		$warna_kategori = ($warna_kategori !=='') ? $warna_kategori  : '#333333';
		?>

		<span class="post-cart"><a title="Lihat artikel lainnya pada kategori <?php echo $category->name ; ?>" style="color:<?php echo $warna_kategori; ?>" href="<?php echo get_category_link( $category->term_id ); ?>" >
		<i class="fas fa-caret-right"></i> <?php echo $category->name ; ?></a></span>
		

	<?php }

}

function nama_kategori_text() {

	$terms = get_the_terms($post->ID, 'category');
	foreach( $terms as $category) { 

		$warna_kategori = get_term_meta($category->term_id, 'warna_kategori', true);
		$warna_kategori = ($warna_kategori !=='') ? $warna_kategori  : '#333333';
		?>

		<a class="meta-1" title="Lihat artikel lainnya pada kategori <?php echo $category->name ; ?>" style="color:<?php echo $warna_kategori; ?>" href="<?php echo get_category_link( $category->term_id ); ?>" >
             <?php echo $category->name ; ?></a>
		

	<?php }

}

function popular_tag() { ?>
	<div class="popular-tag-sidebar">
        <div class="section-title mb-3 mb-lg-4">
            <h4>Tag Populer</h4>
        </div>
        <div class="popular-tag">
            <?php
				$tags = get_tags();
				$args = array(
					'smallest' => 14,
					'largest' => 14,
					'unit' => 'px',
					'number' => 10,
					'format' => 'flat',
					'separator' => " ",
					'orderby' => 'count',
					'order' => 'DESC',
					'show_count' => 1,
					'echo' => false
				);

				$tag_string = wp_generate_tag_cloud( $tags, $args );

				return $tag_string;
            ?>
        </div>
        

<?php 
}

