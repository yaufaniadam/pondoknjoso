<?php
add_action( 'init', 'create_post_type_quote' );
function create_post_type_quote() {
	
	// CPT Quotes	
	register_post_type( 'quotes',
		array(
		  'labels' => array(
			'name' => __( 'Quotes', 'sa' ),
			'singular_name' => __( 'Quote', 'sa' ),	
			'add_new' => _x( 'Add New', 'Quote', 'sa' ),
			'add_new_item' => __( 'Add New Quote', 'sa' ),
			'edit_item' => __( 'Edit Quote', 'sa' ),
			'new_item' => __( 'New Quote', 'sa' ),
			'view_item' => __( 'View Quote', 'sa' ),
			'search_items' => __( 'Search Quote', 'sa' ),
			'not_found' =>  __( 'No Quote found', 'sa' ),
			'not_found_in_trash' => __( 'No Quote found in Trash', 'sa' ),
			'parent_item_colon' => ''
		  ),
		  'public' => true,
		  'has_archive' => true,
		  'supports' => array('title'),
		  'query_var' => true,
		  'show_in_nav_menus' => false,		  
		  'menu_icon' => 'dashicons-format-quote',
		  'menu_position' => 20,
		  'rewrite' => array( 'slug' => 'quote' ),
		)
	);
	
}
add_action( 'init', 'create_post_type_majalah' );
function create_post_type_majalah() {
	
	// CPT majalahs	
	register_post_type( 'majalah',
		array(
		  'labels' => array(
			'name' => __( 'Majalah', 'sa' ),
			'singular_name' => __( 'majalah', 'sa' ),	
			'add_new' => _x( 'Add New', 'majalah', 'sa' ),
			'add_new_item' => __( 'Add New Majalah', 'sa' ),
			'edit_item' => __( 'Edit majalah', 'sa' ),
			'new_item' => __( 'New majalah', 'sa' ),
			'view_item' => __( 'View majalah', 'sa' ),
			'search_items' => __( 'Search majalah', 'sa' ),
			'not_found' =>  __( 'No majalah found', 'sa' ),
			'not_found_in_trash' => __( 'No majalah found in Trash', 'sa' ),
			'parent_item_colon' => ''
		  ),
		  'show_in_rest' => true,
		  'public' => true,
		  'has_archive' => true,
		  'supports' => array('title','editor','thumbnail'),
		  'query_var' => true,
		  'show_in_nav_menus' => false,		  
		  'menu_icon' => 'dashicons-book',
		  'menu_position' => 20,
		  'rewrite' => array( 'slug' => 'majalah' ),
		)
	);
	
}

add_action( 'init', 'create_post_type_banner' );
function create_post_type_banner() {
	
	// CPT banner
	register_post_type( 'banner',
		array(
		  'labels' => array(
			'name' => __( 'Banners', 'sa' ),
			'singular_name' => __( 'banner', 'sa' ),	
			'add_new' => _x( 'Add New', 'banner', 'sa' ),
			'add_new_item' => __( 'Add New banner', 'sa' ),
			'edit_item' => __( 'Edit banner', 'sa' ),
			'new_item' => __( 'New banner', 'sa' ),
			'view_item' => __( 'View banner', 'sa' ),
			'search_items' => __( 'Search banner', 'sa' ),
			'not_found' =>  __( 'No banner found', 'sa' ),
			'not_found_in_trash' => __( 'No majalah found in Trash', 'sa' ),
			'parent_item_colon' => ''
		  ),
		  'public' => true,
		  'has_archive' => true,
		  'supports' => array('title'),
		  'query_var' => true,
		  'show_in_nav_menus' => false,		  
		  'menu_icon' => 'dashicons-editor-table',
		  'menu_position' => 20,
		  'rewrite' => array( 'slug' => 'banner' ),
		)
	);
	
}

add_action( 'init', 'create_post_type_product' );
function create_post_type_product() {
	
	// CPT product
	register_post_type( 'product',
		array(
		  'labels' => array(
			'name' => __( 'Products', 'sa' ),
			'singular_name' => __( 'Product', 'sa' ),	
			'add_new' => _x( 'Add New', 'product', 'sa' ),
			'add_new_item' => __( 'Add New Product', 'sa' ),
			'edit_item' => __( 'Edit Product', 'sa' ),
			'new_item' => __( 'New Product', 'sa' ),
			'view_item' => __( 'View Product', 'sa' ),
			'search_items' => __( 'Search Product', 'sa' ),
			'not_found' =>  __( 'No Product found', 'sa' ),
			'not_found_in_trash' => __( 'No Product found in Trash', 'sa' ),
			'parent_item_colon' => ''
		  ),
		  'show_in_rest' => true,
		  'public' => true,
		  'has_archive' => true,
		  'supports' => array('title','editor','thumbnail'),
		  'query_var' => true,
		  'show_in_nav_menus' => false,		  
		  'menu_icon' => 'dashicons-cart',
		  'menu_position' => 20,
		  'rewrite' => array( 'slug' => 'product' ),
		)
	);
	
}