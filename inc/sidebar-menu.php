 <?php 
					wp_nav_menu( array(
						'menu'              => 'primary',
						'theme_location'    => 'primary',
						'depth'             => 3,
						'container'         => 'div',
						'container_id'   	=> 'cssmenu',
						'fallback_cb'       => 'CSS_Menu_Maker_Walker::fallback',			
						'walker'            => new CSS_Menu_Maker_Walker()
						)
					); 
				?>	