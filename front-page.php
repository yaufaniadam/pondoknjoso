<?php get_header(); ?>

            <?php kutipan(); ?>

            <!-- hero-area-start -->
            <div class="hero-area hero-padding pt-3 pt-md-2 pb-2">
                <div class="container">
                    <div class="row">                        
                        
                        <div class="col-xl-6 col-lg-6">

                            <?php
                                $the_query = null;
                                $the_query = new WP_Query('post_status=publish&post_type=post&posts_per_page=1&category__not_in=79'); 
            
                                while ( $the_query->have_posts() ) : $the_query->the_post();
                            ?>
                            <div class="hero-wrapper mb-30">
                                <div class="hero-img pos-rel">
                               

                                      <?php 
                                            if ( has_post_thumbnail() ) {
                                                the_post_thumbnail('headline');                     
                                            } else {
                                        ?> 
                                        <img src="<?php bloginfo('template_directory');?>/images/noimage/headline.jpg" alt="<?php the_title(); ?>" />
                                    <?php } ?>

                                        

                                    <?php nama_kategori_headline(); ?>
                                    <div class="hero-text">
                                        <div class="hero-meta">
                                            <span><a href="#"><i class="far fa-clock"></i> <?php echo get_the_date('j M Y'); ?></a></span>
                                          
                                        </div>
                                        <h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h3>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><span>Selengkapnya</span> <i class="far fa-long-arrow-right"></i></a>
                                    </div>
                                </div>
                            </div>
                            <?php endwhile; wp_reset_query(); ?>
                        </div>

                       

                        <div class="col-xl-6 col-lg-6">
                            <div class="row">

                                <?php
                                    $the_query = null;
                                    $the_query = new WP_Query('post_status=publish&post_type=post&posts_per_page=4&offset=-1&category__not_in=79'); 
                    
                                    while ( $the_query->have_posts() ) : $the_query->the_post();
                                ?>

                                <div class="col-xl-6 col-lg-6 col-md-6 mb-3 mb-lg-4">
                                    <div class="postbox">
                                        <div class="post-img">
                                            
                                            <?php 
                                                if ( has_post_thumbnail() ) {
                                                    the_post_thumbnail('headline-mini');                     
                                                } else {
                                            ?>  <img src="<?php bloginfo('template_directory');?>/images/noimage/headline-mini.jpg" alt="<?php the_title(); ?>" />
                                            <?php } ?>

                                        </div>
                                        <div class="post-box-text mt-0 mt-md-3">
                                            <div class="post-box-meta">
                                                <?php nama_kategori(); ?>
                                                <span class="post-box-cart1"><a href="#"><i class="far fa-calendar-alt"></i> <?php echo get_the_date('j M Y'); ?></a></span>
                                            </div>
                                            <h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>                                          

                                            </h4>
                                        </div>
                                    </div>
                                </div>
                                <?php endwhile; wp_reset_query(); ?>                                
                            </div>
                        </div>          
                    </div>
                </div>
            </div>
            <!-- hero-area-end -->

            <!-- portfolio-area-start -->
            <div class="portfolio-area">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12 mb-30">
                            <div class="section-title mb-3 mb-lg-4">
                                <h4>Berita Populer</h4>
                            </div>
                            <div class="row">
                                <div class="news-04-active owl-carousel">

                                <?php
                                    $the_query = null;
                                    $args = array (
                                        'post_status'   =>'publish',
                                        'post_type'     =>'post',
                                        'category_name' => 'berita',
                                        'posts_per_page'=> 10,
                                        'meta_key'      => 'post_views_count',
                                        'order'         => 'DESC',
                                        'orderby'       => 'meta_value_num',
                                    );
                        
                                    $the_query = new WP_Query($args); 
                    
                                    while ( $the_query->have_posts() ) : $the_query->the_post();
                                ?>

                                    
                                    <div class="col-xl-12">
                                        <div class="hero-wrapper hero-03-wrapper hero-2-wrapper">
                                            <div class="hero-img pos-rel">
                                                <?php 
                                                    if ( has_post_thumbnail() ) {
                                                        the_post_thumbnail('popular');                 
                                                    } else {
                                                ?>     
                                                    <img src="<?php bloginfo('template_directory');?>/images/noimage/popular.jpg" alt="<?php the_title(); ?>" />
                                                <?php } ?>
                                                <div class="hero-text">
                                                    <div class="hero-meta">
                                                        <span><a href="#"><i class="far fa-clock"></i> <?php echo get_the_date('j M Y'); ?></a></span>
                                                    </div>
                                                    <h3><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a> </h3>
                                                   
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <?php endwhile; wp_reset_query(); ?>
                              
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- portfolio-area-end -->

            <!-- banner-Home 1 -->
            <div class="banner-area">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-9">
                            <div class="banner-2-img">
                                <a href="#"><img src="<?php bloginfo('template_directory');?>/images/iklan/banner-muktamar5.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- banner-area-end -->

            <!-- latest-news-area-start -->
            <div class="latest-news-area pt-4 pt-md-5 pb-3">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-8 mb-30">
                            <div class="section-title section-2  mb-3 mb-lg-4">
                                <h4 class="f-left">Terbaru</h4>
                                <div class="latest-newss-button f-right">
                                    <a href="<?php bloginfo('url'); ?>/indeks">Lihat indeks <i class="far fa-long-arrow-right"></i></a>
                                </div>
                            </div>

                            <?php
                                $the_query = null;
                                $the_query = new WP_Query('post_status=publish&post_type=post&posts_per_page=5&offset=-5'); 
                    
                                while ( $the_query->have_posts() ) : $the_query->the_post();
                            ?>

                            <div class="row">
                                <div class="col-xl-5 col-lg-5 col-4  mb-0 mb-lg-4">
                                    <div class="lifestyle-img">                                     

                                         <?php 
                                            if ( has_post_thumbnail() ) {
                                                the_post_thumbnail('latest');                 
                                            } else {
                                        ?>     
                                            <img src="<?php bloginfo('template_directory');?>/images/noimage/latest.jpg" alt="<?php the_title(); ?>" />
                                        <?php } ?>

                                    </div>
                                </div>
                                <div class="col-xl-7 col-lg-7 pl-0 col-8 mb-10 mb-md-5">
                                    <div class="lifestyle-text lifestyle-04-text">
                                         <div class="post-content-meta"><?php nama_kategori(); ?> 
                                            <span><a class="meta-11" href="#"><i class="far fa-clock"></i> <?php echo get_the_date('j M Y'); ?></a></span>
          
                                        </div>
                                        <h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
                                        <p class="d-none d-md-block"><?php echo excerpt('40'); ?></p>
                                       
                                    </div>
                                </div>
                            </div>

                            <?php endwhile; wp_reset_query(); ?>
                        
                        </div>
                        <div class="col-xl-4 col-lg-4 mb-30">          

                           <!-- Banner Home 2 -->                    
                            <div class="banner-2-img mb-30">
                                <a href="#"><img src="<?php bloginfo('template_directory');?>/images/iklan/banner-muktamar3.png" alt=""></a>
                            </div> 
                          
                            <?php echo popular_tag(); ?>                     

                        </div>
                    </div>
                </div>
            </div>
            <!-- latest-news-area-end -->

            <!-- hero-area-start -->
            <div class="hero-area grey-bg pt-lg-5 pb-lg-5  pt-3 pb-0">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-12 col-lg-12">
                            <div class="section-title mb-30">
                                <h4>Majalah</h4>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="news-active owl-carousel">                         
                        <?php
                            $the_query = null;
                            $the_query = new WP_Query('post_status=publish&post_type=majalah&posts_per_page=6'); 
                            
                            while ( $the_query->have_posts() ) : $the_query->the_post();
                        ?>

                            <div class="col-xl-12">
                                <div class="hero-wrapper hero-04-wrapper mb-30">
                                    <div class="hero-img pos-rel">
                                      
                                            
                                        <?php 
                                        if ( has_post_thumbnail() ) {
                                            the_post_thumbnail('magazine');                     
                                        } else {
                                        ?>                                                              
                                            <img src="<?php bloginfo('template_directory');?>/images/noimage/magazine.jpg" alt="<?php the_title(); ?>" />
                                        <?php } ?>
                                    
                                        <span class="d-none d-md-block"><a class="post-cart post-01-cart color-3" href="<?php the_permalink(); ?>"><?php the_title(); ?></a></span>

                                        <div class="hero-text">
                                           
                                            <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                                            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><span>Lihat Majalah</span> <i class="far fa-long-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                            </div>                        
                        <?php endwhile; wp_reset_query(); ?> 
                        </div>
                    </div>
                </div><!-- end row -->
            </div>
            <!-- hero-area-end -->

            <!-- news-03-area-start -->
            <div class="news-03-area pt-3 pt-md-5 pb-3">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-4 col-lg-6 pl-0 pr-0">
                            <?php hero_sidebar('Hikmah', 'hikmah',4); ?>
                        </div>

                        <div class="col-xl-4  col-lg-6 pl-0 pr-0">
                          <?php hero_sidebar2('Wawasan', 'wawasan',3); ?>
                        </div>

                        <div class="col-xl-4 col-lg-6 pl-0 pr-0">
                            <?php hero_sidebar('Gaya Hidup', 'gaya-hidup',4); ?>
                        </div>

                    </div>
                </div>
            </div>
            <!-- news-03-area-end -->

            <!-- news-03-area-start -->
            <div class="news-03-area pt-3 pb-3 pb-md-5">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-4 col-lg-6 pl-0 pr-0">
                          <?php hero_sidebar('Aksara', 'aksara',4); ?>                          
                        </div>

                        <div class="col-xl-4 col-lg-6 pl-0 pr-0">
                            <?php hero_sidebar('Inspirasi', 'inspirasi',4); ?>
                        </div>

                        <div class="col-xl-4 col-lg-6 pl-0 pr-0">
                            <div class="section-title mb-30">
                                <h4>Produk Suara 'Aisyiyah</h4>
                            </div>
                            <div class="banner-2-img mt-30">
                                <a href="<?php bloginfo('url'); ?>/product"><img src="<?php bloginfo('template_directory');?>/images/iklan/produk-sa.jpg" alt=""></a>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- news-03-area-end -->

<?php get_footer(); ?>