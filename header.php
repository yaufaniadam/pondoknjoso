<!DOCTYPE html>
<html class="no-js" lang="id_ID">
    <head>
        <meta charset="<?php bloginfo( 'charset' ); ?>"> 
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title><?php wp_title( '|', true, 'right' ); ?></title>
        <meta name="description" content="<?php bloginfo('description') ;?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="profile" href="http://gmpg.org/xfn/11">
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>"> 

        <link rel="shortcut icon" type="image/x-icon" href="<?php bloginfo('template_directory'); ?>/favicon.ico">

        <?php  wp_head(); ?>  

    </head>
    <body>

        <!-- header-start -->
    <header>  
            <div class="header-middle-area pt-0 pt-md-2 pb-0 pb-md-2 green-aisyiyah-bg">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-3 col-lg-3 d-flex align-items-center">
                            <div class="logo">
                                <a href="<?php bloginfo('url'); ?>" title="Kembali ke Beranda"><img src="<?php bloginfo('template_directory');?>/images/logosa-w.png" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-xl-9 col-lg-9">
                            <span class="text-right d-block">
                                <?php 
                                        wp_nav_menu( array(
                                            'menu'              => 'top-header',
                                            'theme_location'    => 'top-header',
                                            'depth'             => 1,
                                            'container'         => 'div',
                                            'menu_class'    => 'header-top-link f-right d-none d-md-block',
                                            )
                                        ); 
                                ?>  
                            </span>
                            <!-- Banner Header-->
                                <div class="header-banner text-right d-none d-lg-block">
                                <a href="index.html"><img src="<?php bloginfo('template_directory');?>/images/iklan/banner-muktamar.jpg" alt="" /></a>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mobile-menu"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="sticky-header" class="main-menu-area d-none d-lg-block green-aisyiyah-bg-half">
                <div class="container">
                    <div class="menu-bg yellow-bg position-relative">
                        <div class="row">
                            <div class="col-xl-11 col-lg-11 col-md-12 position-static">
                          
                                <div class="main-menu f-left">

                                     <?php 
                                        wp_nav_menu( array(
                                            'menu'              => 'primary',
                                            'theme_location'    => 'primary',
                                            'depth'             => 3,
                                            'container'         => 'nav',
                                            'container_id'      => 'mobile-menu',
                                            'fallback_cb'       => 'CSS_Menu_Maker_Walker::fallback',           
                                            'walker'            => new CSS_Menu_Maker_Walker()
                                            )
                                        ); 
                                    ?>  

                                </div>
                                
                            </div>
                            <div class="col-xl-1 col-lg-1 d-none d-lg-block">
                                <div class="header-1-right">                                    
                                    <div class="f-right d-none d-lg-block search-icon">
                                        <span><a href="#" data-toggle="modal" data-target="#search-modal">
                                            <i class="fas fa-search"></i></a>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

           
        </header>
        <!-- header-start -->

        <main>